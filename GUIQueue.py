from tkinter import Tk, Button,Label,Scrollbar,Listbox,StringVar,Entry,W,E,N,S,END
from tkinter import ttk
from tkinter import messagebox

class Queue:
    '''
    class docs
    Queue class allows user to perform enqueue and dequeue operations
    using queue
    '''
    def __init__(self):
        '''
        constructor
        Queue with instance variable items initialized to an empty list
        '''
        self.items = []

    # displays all the items in the queue
    def display_queue(self):
        return self.items

    # is_empty returns True only if items is empty
    def is_empty(self):
        return self.items == []
 
    # enqueue appends data to items
    def enqueue(self, data):
        self.items.append(data)
 
    # dequeue dequeues the first element in items
    def dequeue(self):
        return self.items.pop(0)
    
q = Queue() # instance of Queue

# command to display the items in the queue
def display_queue():
    list_bx.delete(0, 'end')
    list_bx.insert('end', q.display_queue())

# command to enqueue the item
def enqueue():
    q.enqueue(value_entry.get())
    list_bx.delete(0, 'end')
    list_bx.insert('end', (value_entry.get()))
    value_entry.delete(0, "end") # Clears input after inserting

# command to dequeue the item
def dequeue():
    if q.is_empty():
        messagebox.showinfo("DeQueue", "Queue is empty.")
    else:
        messagebox.showinfo("DeQueue", "Dequeued value: "+q.dequeue()+"")
        list_bx.delete(0, 'end')
        list_bx.insert('end', q.display_queue())

# command to clear the screen
def clear_screen():
    list_bx.delete(0,'end')
    value_entry.delete(0,'end')

# command to close the screen
def on_closing():
    if messagebox.askokcancel("Quit", "Do you want to quit?"):
        root.destroy()


root = Tk()  # Creates application window

root.title("Queue WorkFlow") # Adds a title to application window
root.configure(background="gray11")  # Add background color to application window
root.geometry("550x450")  # Sets a size for application window
root.resizable(width=False,height=False) # Prevents the application window from resizing

# Create Labels and entry widgets

value_label =ttk.Label(root,text="Enter Value",background="turquoise1",font=("TkDefaultFont", 16))
value_label.grid(row=0, column=0, sticky=W , padx = 20 ,pady = 20)
value_text = StringVar()
value_entry = ttk.Entry(root,width=24,textvariable=value_text)
value_entry.grid(row=0, column=1, sticky=W ,padx = 20  ,pady = 20)

# Add a button to insert inputs

add_btn = Button(root, text="Enqueue",bg="blue",fg="white",font="helvetica 10 bold",command=enqueue)
add_btn.grid(row=0, column=2, sticky=W,padx = 20  ,pady = 20)

# Add  a listbox  to display queue
list_bx = Listbox(root,height=8,width=20,font="helvetica 13",bg="light blue")
list_bx.grid(row=3,column=0, columnspan=14,sticky=W + E,pady=40,padx=15)

# Add scrollbar to enable scrolling
scroll_bar = Scrollbar(root)
scroll_bar.grid(row=1,column=8, rowspan=14,sticky=W )

list_bx.configure(yscrollcommand=scroll_bar.set) # Enables vetical scrolling
scroll_bar.configure(command=list_bx.yview)

# Add more Button Widgets

dequeue_btn = Button(root, text="Dequeue",bg="blue",fg="white",font="helvetica 10 bold",command=dequeue)
dequeue_btn.grid(row=1, column=1, sticky=W,padx = 20  ,pady = 20)

display_queue = Button(root, text="Display Queue",bg="blue",fg="white",font="helvetica 10 bold",command=display_queue)
display_queue.grid(row=1, column=2, sticky=W,padx = 20  ,pady = 20)

exit_btn = Button(root, text="Exit  Application",bg="blue",fg="white",font="helvetica 10 bold",command=root.destroy)
exit_btn.grid(row=4, column=1)

root.mainloop()  # Runs the application until exit
