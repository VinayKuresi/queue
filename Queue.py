class Queue:
    '''
    class docs
    Queue class allows user to perform enqueue and dequeue operations
    using queue
    '''
    def __init__(self):
        '''
        constructor
        Queue with instance variable items initialized to an empty list
        '''
        self.items = []

    # displays all the items in the queue
    def display_queue(self):
        return self.items

    # is_empty returns True only if items is empty
    def is_empty(self):
        return self.items == []

    # enqueue appends data to items
    def enqueue(self, data):
        self.items.append(data)
 
    # dequeue dequeues the first element in items
    def dequeue(self):
        return self.items.pop(0)
 
 
q = Queue() # instance of Queue
while True:
    print('\nFor enqueue press 1')
    print('For dequeue press 2')
    print('For displaying the queue press 3')
    print('For quit press 4')
    option = int(input('\nWhat would you like to do? \n'))
 
    if option == 1:
        value = input("\nEnter the value to enqueue : \n")
        q.enqueue(value)
    elif option == 2:
        if q.is_empty():
            print('\nQueue is empty.\n')
        else:
            print('\nDequeued value: \n', q.dequeue())
    elif option == 3:
        print(q.display_queue())
        print()
    elif option == 4:
        break